# OpenML dataset: risk-factors-cervical

https://www.openml.org/d/42911

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Kelwin Fernandes, Jaime S. Cardoso, Jessica Fernandes
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Cervical+cancer+%28Risk+Factors%29) - 2017
**Please cite**: [Paper](https://link.springer.com/chapter/10.1007/978-3-319-58838-4_27)  

**Cervical cancer (Risk Factors) Data Set**

The dataset was collected at 'Hospital Universitario de Caracas' in Caracas, Venezuela. The dataset comprises demographic information, habits, and historic medical records of 858 patients. Several patients decided not to answer some of the questions because of privacy concerns (missing values).

### Attribute information

- (int) Age 
- (int) Number of sexual partners 
- (int) First sexual intercourse (age) 
- (int) Num of pregnancies 
- (bool) Smokes 
- (bool) Smokes (years) 
- (bool) Smokes (packs/year) 
- (bool) Hormonal Contraceptives 
- (int) Hormonal Contraceptives (years) 
- (bool) IUD 
- (int) IUD (years) 
- (bool) STDs 
- (int) STDs (number) 
- (bool) STDs:condylomatosis 
- (bool) STDs:cervical condylomatosis 
- (bool) STDs:vaginal condylomatosis 
- (bool) STDs:vulvo-perineal condylomatosis 
- (bool) STDs:syphilis 
- (bool) STDs:pelvic inflammatory disease 
- (bool) STDs:genital herpes 
- (bool) STDs:molluscum contagiosum 
- (bool) STDs:AIDS 
- (bool) STDs:HIV 
- (bool) STDs:Hepatitis B 
- (bool) STDs:HPV 
- (int) STDs: Number of diagnosis 
- (int) STDs: Time since first diagnosis 
- (int) STDs: Time since last diagnosis 
- (bool) Dx:Cancer 
- (bool) Dx:CIN 
- (bool) Dx:HPV 
- (bool) Dx 
- (bool) Hinselmann: target variable 
- (bool) Schiller: target variable 
- (bool) Cytology: target variable 
- (bool) Biopsy: target variable

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42911) of an [OpenML dataset](https://www.openml.org/d/42911). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42911/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42911/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42911/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

